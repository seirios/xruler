/* 
 * Copyright (C) 2015, 2019 Sirio Bolaños Puchet
 * 
 * This file is part of XRuler
 * 
 * XRuler is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * XRuler is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XRuler. If not, see <http://www.gnu.org/licenses/>.
 */

#include <forms.h>
#include <string.h>
#include <X11/keysym.h>

#define CWIN 1 /* position of 1px window in cursor (from border) */
#include "cur_N.xbm"
#include "cur_E.xbm"
#include "cur_S.xbm"
#include "cur_W.xbm"

#define PSIZE (span * zoom) /* magnification square size */

#define TLEN 10 /* status length */
#define TSTYLE FL_FIXED_STYLE
#define TSIZE FL_TINY_SIZE
#define TW (fl_get_char_width(TSTYLE,TSIZE)) /* character width */
#define TH (ta+td) /* character height */

#define CUR_COLOR FL_FREE_COL1+1
#define FG_COLOR  FL_FREE_COL1+2
#define BG_COLOR  FL_FREE_COL1+3

#define MIN_WIDTH (7+PSIZE+TLEN*TW)
#define MAX_WIDTH fl_scrw
#define MIN_HEIGHT (35+PSIZE)
#define MAX_HEIGHT fl_scrh

#define CURSOR_LENGTH (cur_N_height-1) /* distance between cursor hot and border */

#define ABS(x) ((x) < 0 ? -(x) : (x))
#define MAX(x,y) ((x) > (y) ? (x) : (y))

enum xr_vh { XR_HU, XR_HD, XR_VL, XR_VR };
enum xr_op { XR_OPH, XR_OPL };
enum xr_wl { XR_TOP, XR_UP, XR_DN };
enum xr_of { XR_OFM, XR_OFL };
enum xr_un { XR_PX, XR_IN, XR_CM };

/* TODO: place status on opposite end of cursor
 * (to avoid obfuscating markers) */
/* TODO: variable cursor length */

Pixmap pix;
GC gc;
FL_OBJECT *tlabel,*clabel,*pixmap,*back,*grp;
FL_FORM *form;

int ta,td; /* text ascent and descent */
int cur[4]; /* cursors */
int orient = 0; /* orientation */
int offset = 0; /* scale offset */
int marks[2] = {0,0}; /* marker positions */
Bool markf[2] = {False,False}; /* markers set? */
int markid = 0; /* next marker id */
Bool shown = True; /* status visibility */

int span = 5; /* size of preview */
int zoom = 5; /* level of magnification */

int Ms = 50; /* major marks every this pixels */
int ms = 10; /* minor marks every this pixels */
int ns = 2; /* mini marks every this pixels */
int Ml = 19; /* major mark length */
int ml = 11; /* minor mark length */
int nl = 7; /* mini mark length */
int calib = -1; /* dpi or dpcm */
enum xr_un unit = XR_PX; /* unit */

void change_offset(enum xr_of ml, unsigned int step) {
    offset += ((ml==XR_OFM)?1:-1) * step;
	fl_winbackground(form->window,fl_get_pixel(BG_COLOR));
	fl_redraw_object(back);
}

/* thanks xwininfo! */
void get_rel_winorigin(Window win, FL_Coord *x, FL_Coord *y) {
	XWindowAttributes win_attrib;
	if(XGetWindowAttributes(fl_display,win,&win_attrib)) {
		*x = win_attrib.x;
		*y = win_attrib.y;
	}
}

#define _NET_WM_STATE_REMOVE 0
#define _NET_WM_STATE_ADD 1
#define ATOM_STATE XInternAtom(fl_display,"_NET_WM_STATE",False)
#define ATOM_ABOVE XInternAtom(fl_display,"_NET_WM_STATE_ABOVE",False)
#define ATOM_BELOW XInternAtom(fl_display,"_NET_WM_STATE_BELOW",False)
int windowlayer(Window w,enum xr_wl ud) {
	XEvent xev;
	Atom atom;
	unsigned char *data;
	int format;
	unsigned int i;
	unsigned long n, left;

	switch(ud) {
		case XR_TOP: /* works because window not yet mapped */
			atom = ATOM_ABOVE;
			XChangeProperty(fl_display,w,ATOM_STATE,XA_ATOM,32,PropModeReplace,(unsigned char *)&atom,1);
			return 2;
			break;
		case XR_UP:
		case XR_DN:
			XGetWindowProperty(fl_display,w,ATOM_STATE,0L,1L,False,XA_ATOM,&atom,&format,&n,&left,(unsigned char**)&data);
			if(data != None) {
				xev.xclient.type = ClientMessage;	xev.xclient.serial = 0;
				xev.xclient.send_event = True; 		xev.xclient.display = fl_display;
				xev.xclient.window = w; 			xev.xclient.message_type = ATOM_STATE;
				xev.xclient.format = 32; 			xev.xclient.data.l[2] = 0;
				for(i=0;i<n;i++) {
					if(((Atom*)data)[i] == ((ud==XR_UP)?ATOM_BELOW:ATOM_ABOVE)) {
						xev.xclient.data.l[0] = _NET_WM_STATE_REMOVE;
						xev.xclient.data.l[1] = ((ud==XR_UP)?ATOM_BELOW:ATOM_ABOVE);
						XSendEvent(fl_display,fl_root,False,SubstructureRedirectMask|SubstructureNotifyMask,&xev);
						XFlush(fl_display);
						return 1;
					} else if(((Atom*)data)[i] == ((ud==XR_UP)?ATOM_ABOVE:ATOM_BELOW))
						return (ud==XR_UP)?2:0;
				}
				xev.xclient.data.l[0] = _NET_WM_STATE_ADD;
				xev.xclient.data.l[1] = ((ud==XR_UP)?ATOM_ABOVE:ATOM_BELOW);
				XSendEvent(fl_display,fl_root,False,SubstructureRedirectMask|SubstructureNotifyMask,&xev);
				XFlush(fl_display);
				return (ud==XR_UP)?2:0;
			} else
				return -1;
			break;
	}
	return -1;
}
#undef ATOM_BELOW
#undef ATOM_ABOVE
#undef ATOM_STATE
#undef _NET_WM_STATE_REMOVE
#undef _NET_WM_STATE_ADD

/* undecorate (thanks to devilspie!) */
#define PROP_MOTIF_WM_HINTS_ELEMENTS 5
#define MWM_HINTS_DECORATIONS (1L << 1)
void undecorate(const Window w) {
	Atom atom;
	struct {
		unsigned long flags;
		unsigned long functions;
		unsigned long decorations;
		long inputMode;
		unsigned long status;
	} hints = {0,};

	hints.flags = MWM_HINTS_DECORATIONS;
	hints.decorations = 0;

	/* Motif hints */
	atom = XInternAtom(fl_display,"_MOTIF_WM_HINTS",False);
	XChangeProperty(fl_display,w,atom,XA_ATOM,32,PropModeReplace,(unsigned char*)&hints,PROP_MOTIF_WM_HINTS_ELEMENTS);

	/* OpenBox */
	atom = XInternAtom(fl_display,"_OB_WM_STATE_UNDECORATED",False);
	XChangeProperty(fl_display,w,XInternAtom(fl_display,"_NET_WM_STATE",False),XA_ATOM,32,\
			PropModeAppend,(unsigned char *)&atom,1);
}
#undef PROP_MOTIF_WM_HINTS_ELEMENTS
#undef MWM_HINTS_DECORATIONS

void utilitywindow(const Window w) {
	Atom atom = XInternAtom(fl_display,"_NET_WM_WINDOW_TYPE_UTILITY",False);
	XChangeProperty(fl_display,w,XInternAtom(fl_display,"_NET_WM_WINDOW_TYPE",False),XA_ATOM,32,\
			PropModeReplace,(unsigned char *)&atom,1);
}

/* set opacity (thanks to transset!) */
#define OPAQUE 0xffffffff
#define ATOM_OPACITY XInternAtom(fl_display,"_NET_WM_WINDOW_OPACITY",False)
void set_opacity(const Window w,enum xr_op mode) {
	Atom atom;
	unsigned int op;
	unsigned char *data;
	int format;
	unsigned long n, left;
	double r;

	XGetWindowProperty(fl_display,w,ATOM_OPACITY,0L,1L,False,XA_CARDINAL,&atom,&format,&n,&left,(unsigned char**)&data);
	if(data != None) {
		memcpy(&op,data,sizeof(unsigned int));
		XFree((void*)data);
	} else
		op = OPAQUE;

	r = (double)op/OPAQUE + ((mode == XR_OPH)?0.05:-0.05);
	if(r<0) { r = 0; } if(r>1) { r = 1; }

	op = (unsigned int)(r * OPAQUE);

	if(op == OPAQUE)
		XDeleteProperty(fl_display,w,ATOM_OPACITY);
	else {
		XChangeProperty(fl_display,w,ATOM_OPACITY,XA_CARDINAL,32,PropModeReplace,(unsigned char*)&op,1L);
		XSync(fl_display,False);
	}
}
#undef OPAQUE
#undef ATOM_OPACITY

void set_sizelimits(FL_Coord mw,FL_Coord mh,FL_Coord Mw,FL_Coord Mh) {
	XSizeHints *hints;
	hints = XAllocSizeHints();
	hints->flags = PMinSize | PMaxSize;
	hints->min_width = mw;
	hints->min_height = mh;
	hints->max_width = Mw;
	hints->max_height = Mh;
	XSetWMNormalHints(fl_display,form->window,hints);
	XFree(hints);
}

void set_layout(enum xr_vh vh) {
	fl_freeze_form(form);
	fl_set_object_position(clabel,0,2);
	fl_set_object_position(tlabel,0,2+TH);
	fl_set_object_position(pixmap,(vh==XR_HU||vh==XR_HD)?TLEN*TW+3:((vh==XR_VL)?TW:(TLEN-1)*TW-PSIZE),(vh==XR_HU)?2*TH+ta-1-PSIZE:((vh==XR_HD)?2:2*TH+ta+2));
	fl_set_object_lalign(clabel,(vh==XR_VR)?FL_ALIGN_RIGHT:FL_ALIGN_LEFT);
	fl_set_object_lalign(tlabel,(vh==XR_VR)?FL_ALIGN_RIGHT:FL_ALIGN_LEFT);
	fl_unfreeze_form(form);
}

void draw_scale(FL_Coord x, FL_Coord y, FL_Coord w, FL_Coord h) {
	FL_Coord ox,oy;
	char buf[16];
	int strd,pos,len;

	switch(orient) {
		case 0: /* fall-through */
		case 2:
			while(x < w) {
				ox = x + offset;
				if(Ms > 0 && ox%Ms == 0) {
                    fl_line(x,y+((orient==0)?0:h),x,y+((orient==0)?0:h)+((orient==0)?1:-1)*Ml,FG_COLOR);
                    snprintf(buf,sizeof(buf),"%.4g",ox/((calib>0)?1.0*calib:1));
					fl_draw_text(FL_ALIGN_CENTER,x,y+((orient==0)?Ml+td:h-Ml-td-ta),0,TH,FG_COLOR,TSTYLE,TSIZE,buf);
                }
                else if(ms > 0 && ox%ms == 0)
                    fl_line(x,y+((orient==0)?0:h),x,y+((orient==0)?0:h)+((orient==0)?1:-1)*ml,FG_COLOR);
                else if(ns > 0 && ox%ns == 0)
                    fl_line(x,y+((orient==0)?0:h),x,y+((orient==0)?0:h)+((orient==0)?1:-1)*nl,FG_COLOR);
				x++;
			}
            /* marker related stuff */
			if(markf[0] && marks[0]-offset >= 0 && marks[0]-offset < w)
				fl_line(marks[0]-offset,y,marks[0]-offset,y+h-1,CUR_COLOR);
			if(markf[1] && marks[1]-offset >= 0 && marks[1]-offset < w)
				fl_line(marks[1]-offset,y,marks[1]-offset,y+h-1,CUR_COLOR);
			if(markf[0] && markf[1]) {
				len = snprintf(buf,sizeof(buf),"%.4g",(ABS(marks[0]-marks[1])+((calib>0)?0:1))/((calib>0)?1.0*calib:1));
				strd = fl_get_string_width(TSTYLE,TSIZE,buf,len);
				pos = MAX(marks[0],marks[1]) - offset;
				if(pos+strd+TW < w)
					fl_draw_text(FL_ALIGN_LEFT,pos,((orient==0)?y+h-TH-td:td),0,TH,CUR_COLOR,TSTYLE,TSIZE,buf);
				else
					fl_draw_text(FL_ALIGN_RIGHT,pos,((orient==0)?y+h-TH-td:td),0,TH,CUR_COLOR,TSTYLE,TSIZE,buf);
			} else if(markf[0] && !markf[1]) {
				len = snprintf(buf,sizeof(buf),"%.4g",marks[0]/((calib>0)?1.0*calib:1));
				strd = fl_get_string_width(TSTYLE,TSIZE,buf,len);
				pos = marks[0] - offset;
				if(pos+strd+TW < w)
					fl_draw_text(FL_ALIGN_LEFT,pos,((orient==0)?y+h-TH-td:td),0,TH,CUR_COLOR,TSTYLE,TSIZE,buf);
				else
					fl_draw_text(FL_ALIGN_RIGHT,pos,((orient==0)?y+h-TH-td:td),0,TH,CUR_COLOR,TSTYLE,TSIZE,buf);
			}
			break;	
		case 1: /* fall-through */
		case 3:
			while(y < h) {
				oy = y + offset;
				if(Ms > 0 && oy%Ms == 0) {
                    fl_line(x+((orient==1)?w:0),y,x+((orient==1)?w:0)+((orient==1)?-1:1)*Ml,y,FG_COLOR);
                    snprintf(buf,sizeof(buf),"%.4g",oy/((calib>0)?1.0*calib:1));
					fl_draw_text(((orient==1)?FL_ALIGN_RIGHT:FL_ALIGN_LEFT),x+((orient==1)?w-Ml+2-4*TW:Ml),y,4*TW,0,FG_COLOR,TSTYLE,TSIZE,buf);
                }
                else if(ms > 0 && oy%ms == 0)
                    fl_line(x+((orient==1)?w:0),y,x+((orient==1)?w:0)+((orient==1)?-1:1)*ml,y,FG_COLOR);
                else if(ns > 0 && oy%ns == 0)
                    fl_line(x+((orient==1)?w:0),y,x+((orient==1)?w:0)+((orient==1)?-1:1)*nl,y,FG_COLOR);
				y++;
			}
            /* marker related stuff */
			if(markf[0] && marks[0]-offset >= 0 && marks[0]-offset < h)
				fl_line(x,marks[0]-offset,x+w-1,marks[0]-offset,CUR_COLOR);
			if(markf[1] && marks[1]-offset >= 0 && marks[1]-offset < h)
				fl_line(x,marks[1]-offset,x+w-1,marks[1]-offset,CUR_COLOR);
			if(markf[0] && markf[1]) {
				len = snprintf(buf,sizeof(buf),"%.4g",(ABS(marks[0]-marks[1])+((calib>0)?0:1))/((calib>0)?1.0*calib:1));
				strd = fl_get_string_height(TSTYLE,TSIZE,buf,len,NULL,NULL);
				pos = MAX(marks[0],marks[1]) - offset;
				if(pos+3+strd < h)
					fl_draw_text(((orient==1)?FL_ALIGN_LEFT:FL_ALIGN_RIGHT),((orient==1)?-2:w+2),pos+3,0,TH,CUR_COLOR,TSTYLE,TSIZE,buf);
				else
					fl_draw_text(((orient==1)?FL_ALIGN_LEFT:FL_ALIGN_RIGHT),((orient==1)?-2:w+2),pos-strd-2,0,TH,CUR_COLOR,TSTYLE,TSIZE,buf);
			} else if(markf[0] && !markf[1]) {
				len = snprintf(buf,sizeof(buf),"%.4g",marks[0]/((calib>0)?1.0*calib:1));
				strd = fl_get_string_height(TSTYLE,TSIZE,buf,len,NULL,NULL);
				pos = marks[0] - offset;
				if(pos+3+strd < h)
					fl_draw_text(((orient==1)?FL_ALIGN_LEFT:FL_ALIGN_RIGHT),((orient==1)?-2:w+2),pos+3,0,TH,CUR_COLOR,TSTYLE,TSIZE,buf);
				else
					fl_draw_text(((orient==1)?FL_ALIGN_LEFT:FL_ALIGN_RIGHT),((orient==1)?-2:w+2),pos-strd-2,0,TH,CUR_COLOR,TSTYLE,TSIZE,buf);
			}
			break;	
	}
}

unsigned long get_pixel_at(FL_Coord x,FL_Coord y) {
	XImage *xim;
	unsigned long p;

	xim = XGetImage(fl_display,fl_root,x,y,1,1,AllPlanes,ZPixmap);
	if(xim == None) { return 0xff000000L; }
	p = XGetPixel(xim,0,0);
	XDestroyImage(xim);
	return p;
}

int handle_free(FL_OBJECT* obj, int event, FL_Coord mx FL_UNUSED_ARG,\
		FL_Coord my FL_UNUSED_ARG, int key FL_UNUSED_ARG, void *xev FL_UNUSED_ARG) {
	FL_Coord x,y,w,h;

	if(event == FL_DRAW) {
		fl_get_object_bbox(obj,&x,&y,&w,&h);
		fl_winset(form->window);
		draw_scale(x,y,w,h);
	}
	return 0;
}

void update_position() {
	FL_Coord mx,my,x,y;
	unsigned int kk;

	fl_get_mouse(&mx,&my,&kk);
	fl_get_winorigin(form->window,&x,&y);
	fl_set_object_label_f(tlabel,"%.4g %s",(offset+((orient%2==0)?mx-x:my-y)/((calib>0)?1.0*calib:1)),(unit==XR_IN)?"in":((unit==XR_CM)?"cm":"px"));
}

void set_clabel(unsigned long p) {
	XColor res;
	res.pixel = p;

	XQueryColor(fl_display,fl_colormap,&res);
	fl_set_object_label_f(clabel,"#%.2X%.2X%.2X",res.red/256,res.green/256,res.blue/256);
}

void set_pixmap(unsigned long p) {
	XSetForeground(fl_display,gc,(p&0x00ffffffL)+0xff000000L);
	XFillRectangle(fl_display,pix,gc,0,0,PSIZE,PSIZE);
	fl_set_pixmap_pixmap(pixmap,pix,None);
}

void set_marker() {
	FL_Coord x,y;
	unsigned int kk;

	fl_get_win_mouse(form->window,&x,&y,&kk);
	marks[markid] = (orient%2 == 0) ? x+offset : y+offset;
	markf[markid] = True;
	markid = 1 - markid; /* 0->1 1->0 */
	fl_winbackground(form->window,fl_get_pixel(BG_COLOR));
	fl_redraw_object(back);
}

void draw_magnified(XImage *xim) {
	int i,j;

	for(i=0;i<span;i++)
		for(j=0;j<span;j++) {
			XSetForeground(fl_display,gc,(XGetPixel(xim,i,j)&0x00ffffffL)+0xff000000L); /* alpha 1.0 */
			XFillRectangle(fl_display,pix,gc,i*zoom,j*zoom,zoom,zoom);
		}
}

void draw_square() {
	XSetFunction(fl_display,gc,GXinvert);
	XDrawRectangle(fl_display,pix,gc,((span/2)*zoom)-1,((span/2)*zoom)-1,zoom+1,zoom+1);
	XDrawPoint(fl_display,pix,gc,((span/2)*zoom)-1,((span/2)*zoom)-1); /* fix for nw corner */
	XSetFunction(fl_display,gc,GXcopy);
}

void update_image(FL_Coord mx, FL_Coord my, unsigned long *p) {
	XImage *xim = None;

	if(orient==0 && my >= CURSOR_LENGTH+span/2-CWIN && mx >= span/2 && mx < fl_scrw-span/2)
		xim = XGetImage(fl_display,fl_root,mx-span/2,my-CURSOR_LENGTH-span/2+CWIN,span,span,AllPlanes,ZPixmap);
	else if(orient==1 && mx < fl_scrw-CURSOR_LENGTH-span/2+CWIN && my >= span/2 && my < fl_scrh-span/2)
		xim = XGetImage(fl_display,fl_root,mx+CURSOR_LENGTH-span/2-CWIN,my-span/2,span,span,AllPlanes,ZPixmap);
	else if(orient==2 && my < fl_scrh-CURSOR_LENGTH-span/2+CWIN && mx >= span/2 && mx < fl_scrw-span/2)
		xim = XGetImage(fl_display,fl_root,mx-span/2,my+CURSOR_LENGTH-span/2-CWIN,span,span,AllPlanes,ZPixmap);
	else if(orient==3 && mx >= CURSOR_LENGTH+span/2-CWIN && my >= span/2 && my < fl_scrh-span/2)
		xim = XGetImage(fl_display,fl_root,mx-CURSOR_LENGTH-span/2+CWIN,my-span/2,span,span,AllPlanes,ZPixmap);

	if(xim != None) {
		*p = XGetPixel(xim,span/2,span/2); /* get center pixel */
		draw_magnified(xim);
		XDestroyImage(xim);
		draw_square();
		fl_set_pixmap_pixmap(pixmap,pix,None);
	} else
		set_pixmap(*p);
}

void update_pixel(FL_Coord mx, FL_Coord my, unsigned long *p) {
	if(orient == 0 && my >= CURSOR_LENGTH-CWIN)
		*p = get_pixel_at(mx,my-CURSOR_LENGTH+CWIN);
	else if(orient == 1 && mx < fl_scrw-CURSOR_LENGTH+CWIN)
		*p = get_pixel_at(mx+CURSOR_LENGTH-CWIN,my);
	else if(orient == 2 && my < fl_scrh-CURSOR_LENGTH+CWIN)
		*p = get_pixel_at(mx,my+CURSOR_LENGTH-CWIN);
	else if(orient == 3 && mx >= CURSOR_LENGTH-CWIN)
		*p = get_pixel_at(mx-CURSOR_LENGTH+CWIN,my);
	set_pixmap(*p);
}

void handle_rotation(FL_Coord ww, FL_Coord wh) {
	if(orient%2==0) {
		fl_set_object_gravity(grp,FL_West,(orient==0)?FL_South:FL_North);
		set_sizelimits(MIN_WIDTH,MIN_HEIGHT,MAX_WIDTH,MAX_HEIGHT);
		set_layout((orient==0)?XR_HU:XR_HD);
		fl_winresize(form->window,wh,ww);
		fl_move_object(grp,0,(orient==0)?wh-30:1);
	} else if(orient%2==1) {
		fl_set_object_gravity(grp,(orient==1)?FL_West:FL_East,FL_South);
		set_sizelimits(MIN_HEIGHT,MIN_WIDTH,MAX_WIDTH,MAX_HEIGHT);
		set_layout((orient==1)?XR_VL:XR_VR);
		fl_winresize(form->window,wh,ww);
		fl_move_object(grp,(orient==1)?-3:ww-TLEN*TW+3,wh-PSIZE-2*(TH+td)-ta-1);
	}
}

int handle_form(FL_FORM *form, void *ev) {
	FL_Coord mx,my;
	FL_Coord x,y,ww,wh;
	char buf[16];
	static unsigned long p;
	static Bool over = False;
	XEvent evt;
	unsigned int kk;

	switch(((XEvent*)ev)->type) {
		case MotionNotify:
			if(over == True) {
				update_position();
				fl_get_mouse(&mx,&my,&kk);
				p = 0xff000000L;
				if(((XMotionEvent*)ev)->state & Button3Mask)
					update_image(mx,my,&p);
				else
					update_pixel(mx,my,&p);
				set_clabel(p);
			} 
			break;
		case EnterNotify:
			over = True;
			fl_set_cursor(form->window,cur[orient]);
			if(shown == True) { fl_show_object(grp); }
			fl_get_mouse(&mx,&my,&kk);
			p = 0xff000000L;
			if(((XCrossingEvent*)ev)->state & Button3Mask)
				update_image(mx,my,&p);
			else
				update_pixel(mx,my,&p);
			set_clabel(p);
			break;
		case LeaveNotify:
			fl_get_mouse(&mx,&my,&kk);
			fl_get_wingeometry(form->window,&x,&y,&ww,&wh);
			if(my < y || my >= y+wh || mx < x || mx >= x+ww) {
				over = False;
				fl_set_cursor(form->window,FL_DEFAULT_CURSOR);
				fl_hide_object(grp);
			}
			break;
		case ButtonRelease:
			switch(((XButtonEvent*)ev)->button) {
				case 3:
					set_clabel(p);
					set_pixmap(p);
					break;
			}
			break;
		case ButtonPress:
			switch(((XButtonEvent*)ev)->button) {
				case 1:
					fl_get_mouse(&mx,&my,&kk);
					/* dirty native move */
					XUngrabPointer(fl_display,0L);
					XFlush(fl_display);

					evt.xclient.type         = ClientMessage;
					evt.xclient.window       = form->window;
					evt.xclient.message_type = XInternAtom(fl_display,"_NET_WM_MOVERESIZE",False);
					evt.xclient.format       = 32;
					evt.xclient.data.l[0]    = mx;
					evt.xclient.data.l[1]    = my;
					evt.xclient.data.l[2]    = 8;    /* _NET_WM_MOVERESIZE_MOVE */
					evt.xclient.data.l[3]    = 0;
					evt.xclient.data.l[4]    = 0;

					XSendEvent(fl_display,fl_root,False,SubstructureRedirectMask|SubstructureNotifyMask,&evt);
					XSync(fl_display,False);
					break;
				case 2:
					set_marker();
					fl_get_mouse(&mx,&my,&kk);
					p = 0xff000000L;
					if(((XButtonEvent*)ev)->state & Button3Mask)
						update_image(mx,my,&p);
					else
						update_pixel(mx,my,&p);
					set_clabel(p);
					break;
				case 3:
					fl_get_mouse(&mx,&my,&kk);
					p = 0xff000000L;
					update_image(mx,my,&p);
					break;
				case 4: /* wheel up */
					fl_get_winsize(form->window,&ww,&wh);
					if(orient%2==0) {
						if(shiftkey_down(((XMotionEvent*)ev)->state)) {
							if(ww+1 <= fl_scrw) {
								fl_get_winorigin(form->window,&x,&y);
								get_rel_winorigin(form->window,&mx,&my);
								fl_winreshape(form->window,x-mx-1,y-my,ww+1,wh);
							}
						} else if(controlkey_down(((XMotionEvent*)ev)->state)) {
							change_offset(XR_OFM,1);
							update_position();
						} else {
							if(ww+1 <= fl_scrw)
								fl_winresize(form->window,ww+1,wh);
						}
					} else if(orient%2==1) {
						if(shiftkey_down(((XMotionEvent*)ev)->state)) {
							if(wh+1 <= fl_scrh) {
								fl_get_winorigin(form->window,&x,&y);
								get_rel_winorigin(form->window,&mx,&my);
								fl_winreshape(form->window,x-mx,y-my-1,ww,wh+1);
							}
						} else if(controlkey_down(((XMotionEvent*)ev)->state)) {
							change_offset(XR_OFM,1);
							update_position();
						} else {
							if(wh+1 <= fl_scrh)
								fl_winresize(form->window,ww,wh+1);
						}
					}
					break;
				case 5: /* wheel down */
					fl_get_winsize(form->window,&ww,&wh);
					if(orient%2==0) {
						if(shiftkey_down(((XMotionEvent*)ev)->state)) {
							if(ww-1 >= MIN_WIDTH) {
								fl_get_winorigin(form->window,&x,&y);
								get_rel_winorigin(form->window,&mx,&my);
								fl_winreshape(form->window,x-mx+1,y-my,ww-1,wh);
							}
						} else if(controlkey_down(((XMotionEvent*)ev)->state)) {
							change_offset(XR_OFL,1);
							update_position();
						} else {
							if(ww-1 >= MIN_WIDTH)
								fl_winresize(form->window,ww-1,wh);
						}
					} else if(orient%2==1) {
						if(shiftkey_down(((XMotionEvent*)ev)->state)) {
							if(wh-1 >= MIN_WIDTH) {
								fl_get_winorigin(form->window,&x,&y);
								get_rel_winorigin(form->window,&mx,&my);
								fl_winreshape(form->window,x-mx,y-my+1,ww,wh-1);
							}
						} else if(controlkey_down(((XMotionEvent*)ev)->state)) {
							change_offset(XR_OFL,1);
							update_position();
						} else {
							if(wh-1 >= MIN_WIDTH)
								fl_winresize(form->window,ww,wh-1);
						}
					}
					break;
			}
			break;
		case KeyPress:
			switch(XLookupKeysym((XKeyEvent*)ev,0)) {
				case XK_q:
				case XK_Escape:
					XFreeGC(fl_display,gc);
					fl_finish();
					break;
				case XK_f:
					orient = (orient+2)%4;
					fl_get_wingeometry(form->window,&x,&y,&ww,&wh);
					if(orient%2 == 0) {
						fl_set_object_gravity(grp,FL_West,(orient==0)?FL_South:FL_North);
						set_layout((orient==0)?XR_HU:XR_HD);
						fl_move_object(grp,0,(orient==0)?wh-PSIZE/2-3:0);
					} else if(orient%2==1) {
						fl_set_object_gravity(grp,(orient==1)?FL_West:FL_East,FL_South);
						set_layout((orient==1)?XR_VL:XR_VR);
						fl_move_object(grp,(orient==1)?-3:ww-TLEN*TW+3,wh-PSIZE-2*(TH+td)-ta-1);
					}
					fl_set_cursor(form->window,cur[orient]);
					fl_winbackground(form->window,fl_get_pixel(BG_COLOR));
					fl_redraw_object(back);
					if(over == True) {
						p = 0xff000000L;
						fl_get_mouse(&mx,&my,&kk);
						if(((XKeyEvent*)ev)->state & Button3Mask)
							update_image(mx,my,&p);
						else
							update_pixel(mx,my,&p);
						set_clabel(p);
					}
					break;
				case XK_r:
					orient = (orient+1)%4;
					fl_get_wingeometry(form->window,&x,&y,&ww,&wh);
					handle_rotation(ww,wh);
					fl_set_cursor(form->window,cur[orient]);
					fl_redraw_object(back);
					if(over == True) {
						p = 0xff000000L;
						fl_get_mouse(&mx,&my,&kk);
						if(((XKeyEvent*)ev)->state & Button3Mask)
							update_image(mx,my,&p);
						else
							update_pixel(mx,my,&p);
						set_clabel(p);
					}
					break;
				case XK_y:
					fl_stuff_clipboard(clabel,XA_PRIMARY,fl_get_object_label(clabel),TLEN,NULL);
					break;
				case XK_l:
					if(markf[0] && markf[1]) {
                        snprintf(buf,sizeof(buf),"%.4g",(ABS(marks[0]-marks[1])+((calib>0)?0:1))/((calib>0)?1.0*calib:1));
						fl_stuff_clipboard(clabel,XA_PRIMARY,buf,strlen(buf),NULL);
					} else if(markf[0] && !markf[1]) {
                        snprintf(buf,sizeof(buf),"%.4g",marks[0]/((calib>0)?1.0*calib:1));
						fl_stuff_clipboard(clabel,XA_PRIMARY,buf,strlen(buf),NULL);
					} else if(!markf[0] && !markf[1]) {
						fl_get_wingeometry(form->window,&x,&y,&ww,&wh);
						snprintf(buf,sizeof(buf),"%.4g",((orient%2==0)?ww:wh)/((calib>0)?1.0*calib:1));
						fl_stuff_clipboard(clabel,XA_PRIMARY,buf,strlen(buf),NULL);
					}
					break;
				case XK_c: /* clear markers */
					marks[0] = marks[1] = 0;
					markf[0] = markf[1] = False;
					markid = 0;
					fl_winbackground(form->window,fl_get_pixel(BG_COLOR));
					fl_redraw_object(back);
					break;
				case XK_v: /* toggle status visibility */
					if(shown == True) {
						shown = False;
						fl_hide_object(grp);
					} else {
						shown = True;
						if(over == True) {
							fl_show_object(grp);
							fl_get_mouse(&mx,&my,&kk);
							p = 0xff000000L;
							if(((XKeyEvent*)ev)->state & Button3Mask)
								update_image(mx,my,&p);
							else
								update_pixel(mx,my,&p);
							set_clabel(p);
						}
					}
					break;
				case XK_plus:
				case XK_KP_Add:
					if(shiftkey_down(((XKeyEvent*)ev)->state))
						windowlayer(form->window,XR_UP); /* move up in layer stack */
					else
						set_opacity(form->window,XR_OPH); /* more opaque */
					break;
				case XK_minus:
				case XK_KP_Subtract:
					if(shiftkey_down(((XKeyEvent*)ev)->state))
						windowlayer(form->window,XR_DN); /* move down in layer stack */
					else
						set_opacity(form->window,XR_OPL); /* less opaque */
					break;
				case XK_Up:
					fl_get_wingeometry(form->window,&x,&y,&ww,&wh);
					get_rel_winorigin(form->window,&mx,&my);
					if(shiftkey_down(((XKeyEvent*)ev)->state))
						fl_winresize(form->window,ww,FL_clamp(wh-1,MIN_HEIGHT,MAX_HEIGHT));
					else if(controlkey_down(((XKeyEvent*)ev)->state) && orient%2==1) {
						change_offset(XR_OFM,1);
						update_position();
					} else {
						fl_winmove(form->window,x-mx,y-my-1);
						update_position();
					}
					if(over == True) {
						fl_get_mouse(&mx,&my,&kk);
						p = 0xff000000L;
						if(((XKeyEvent*)ev)->state & Button3Mask)
							update_image(mx,my,&p);
						else
							update_pixel(mx,my,&p);
						set_clabel(p);
					}
					break;
				case XK_Right:
					fl_get_wingeometry(form->window,&x,&y,&ww,&wh);
					get_rel_winorigin(form->window,&mx,&my);
					if(shiftkey_down(((XKeyEvent*)ev)->state)) {
						fl_winresize(form->window,FL_clamp(ww+1,MIN_WIDTH,MAX_WIDTH),wh);
					} else if(controlkey_down(((XKeyEvent*)ev)->state) && orient%2==0) {
						change_offset(XR_OFL,1);
						update_position();
					} else {
						fl_winmove(form->window,x-mx+1,y-my);
						update_position();
					}
					if(over == True) {
						fl_get_mouse(&mx,&my,&kk);
						p = 0xff000000L;
						if(((XKeyEvent*)ev)->state & Button3Mask)
							update_image(mx,my,&p);
						else
							update_pixel(mx,my,&p);
						set_clabel(p);
					}
					break;
				case XK_Down:
					fl_get_wingeometry(form->window,&x,&y,&ww,&wh);
					get_rel_winorigin(form->window,&mx,&my);
					if(shiftkey_down(((XKeyEvent*)ev)->state)) {
						fl_winresize(form->window,ww,FL_clamp(wh+1,MIN_HEIGHT,MAX_HEIGHT));
					} else if(controlkey_down(((XKeyEvent*)ev)->state) && orient%2==1) {
						change_offset(XR_OFL,1);
						update_position();
					} else {
						fl_winmove(form->window,x-mx,y-my+1);
						update_position();
					}
					if(over == True) {
						fl_get_mouse(&mx,&my,&kk);
						p = 0xff000000L;
						if(((XKeyEvent*)ev)->state & Button3Mask)
							update_image(mx,my,&p);
						else
							update_pixel(mx,my,&p);
						set_clabel(p);
					}
					break;
				case XK_Left:
					fl_get_wingeometry(form->window,&x,&y,&ww,&wh);
					get_rel_winorigin(form->window,&mx,&my);
					if(shiftkey_down(((XKeyEvent*)ev)->state))
						fl_winresize(form->window,FL_clamp(ww-1,MIN_WIDTH,MAX_WIDTH),wh);
					else if(controlkey_down(((XKeyEvent*)ev)->state) && orient%2==0) {
						change_offset(XR_OFM,1);
						update_position();
					} else {
						fl_winmove(form->window,x-mx-1,y-my);
						update_position();
					}
					if(over == True) {
						fl_get_mouse(&mx,&my,&kk);
						p = 0xff000000L;
						if(((XKeyEvent*)ev)->state & Button3Mask)
							update_image(mx,my,&p);
						else
							update_pixel(mx,my,&p);
						set_clabel(p);
					}
					break;
				case XK_space:
					if(over == True) {
						set_marker();
						fl_get_mouse(&mx,&my,&kk);
						p = 0xff000000L;
						if(((XKeyEvent*)ev)->state & Button3Mask)
							update_image(mx,my,&p);
						else
							update_pixel(mx,my,&p);
						set_clabel(p);
					}
					break;
			}
			break;
	}
	return FL_PREEMPT;
}

int main (int argc, char *argv[]) {
    static int dpi,dpcm;
	static int init_len,init_thick;
	static char fg[32],bg[32],cc[32];
    static const int nopts = 17;

	FL_CMD_OPT cmdopt[] = {
		{"-offset", ".offset", XrmoptionSepArg, 0},
		{"-orientation", ".orientation", XrmoptionSepArg, 0},
		{"-length", ".length", XrmoptionSepArg, 0},
		{"-thickness",".thickness", XrmoptionSepArg, 0},
		{"-foreground", ".foreground", XrmoptionSepArg, 0},
		{"-background", ".background", XrmoptionSepArg, 0},
		{"-cursorColor", ".cursorColor", XrmoptionSepArg, 0},
		{"-majorStep", ".majorStep", XrmoptionSepArg, 0},
		{"-minorStep", ".minorStep", XrmoptionSepArg, 0},
		{"-miniStep", ".miniStep", XrmoptionSepArg, 0},
		{"-majorLength", ".majorLength", XrmoptionSepArg, 0},
		{"-minorLength", ".minorLength", XrmoptionSepArg, 0},
		{"-miniLength", ".miniLength", XrmoptionSepArg, 0},
		{"-dpi", ".dpi", XrmoptionSepArg, 0},
		{"-dpcm", ".dpcm", XrmoptionSepArg, 0},
		{"-span", ".span", XrmoptionSepArg, 0},
		{"-zoom", ".zoom", XrmoptionSepArg, 0}
	};

	FL_resource res[] = {
		{"offset", "", FL_INT, &offset, "0", 0},
		{"orientation", "", FL_INT, &orient, "0", 0},
		{"length", "", FL_INT, &init_len, "400", 0},
		{"thickness", "", FL_INT, &init_thick, "60", 0},
		{"foreground", "", FL_STRING, &fg, "green", 32},
		{"background", "", FL_STRING, &bg, "black", 32},
		{"cursorColor", "", FL_STRING, &cc, "red", 32},
		{"majorStep", "", FL_INT, &Ms, "50", 0},
		{"minorStep", "", FL_INT, &ms, "10", 0},
		{"miniStep", "", FL_INT, &ns, "2", 0},
		{"majorLength", "", FL_INT, &Ml, "19", 0},
		{"minorLength", "", FL_INT, &ml, "11", 0},
		{"miniLength", "", FL_INT, &nl, "7", 0},
		{"dpi", "", FL_INT, &dpi, "-1", 0},
		{"dpcm", "", FL_INT, &dpcm, "-1", 0},
		{"span", "", FL_INT, &span, "5", 0},
		{"zoom", "", FL_INT, &zoom, "5", 0}
	};

	fl_initialize(&argc,argv,"XRuler",cmdopt,nopts);
	fl_get_app_resources(res,nopts);

    /* process arguments */
	init_len = FL_clamp(init_len,MIN_WIDTH,MAX_WIDTH);
	init_thick = FL_clamp(init_thick,MIN_HEIGHT,MAX_HEIGHT);
	orient = ABS(orient) % 4;
	if(fl_mapcolorname(CUR_COLOR,cc) == -1)
		fl_mapcolor(CUR_COLOR,255,0,0); /* red */
	if(fl_mapcolorname(FG_COLOR,fg) == -1)
		fl_mapcolor(FG_COLOR,0,255,0); /* green */
	if(fl_mapcolorname(BG_COLOR,bg) == -1)
		fl_mapcolor(BG_COLOR,0,0,0); /* black */
    if(dpi > 0) {
        unit = XR_IN;
        calib = dpi;
        Ms = dpi;
        ms = (dpi%4==0)?dpi/4:0; /* deactivate if not exact */
        ns = (dpi%8==0)?dpi/8:0; /* deactivate if not exact */
    } else if(dpcm > 0) {
        unit = XR_CM;
        calib = dpcm;
        Ms = dpcm;
        ms = 0;
        ns = (dpcm%10==0)?dpcm/10:0; /* deactivate if not exact */
    }

	/* load cursors */
	cur[0] = fl_create_bitmap_cursor((char *)cur_N_bits,(char *)cur_N_bits,cur_N_width,cur_N_height,cur_N_x_hot,cur_N_y_hot);
	cur[1] = fl_create_bitmap_cursor((char *)cur_E_bits,(char *)cur_E_bits,cur_E_width,cur_E_height,cur_E_x_hot,cur_E_y_hot);
	cur[2] = fl_create_bitmap_cursor((char *)cur_S_bits,(char *)cur_S_bits,cur_S_width,cur_S_height,cur_S_x_hot,cur_S_y_hot);
	cur[3] = fl_create_bitmap_cursor((char *)cur_W_bits,(char *)cur_W_bits,cur_W_width,cur_W_height,cur_W_x_hot,cur_W_y_hot);
	/* set cursor color */
	fl_set_cursor_color(cur[0],CUR_COLOR,FL_BLACK);
	fl_set_cursor_color(cur[1],CUR_COLOR,FL_BLACK);
	fl_set_cursor_color(cur[2],CUR_COLOR,FL_BLACK);
	fl_set_cursor_color(cur[3],CUR_COLOR,FL_BLACK);

	/* define form */
	form = fl_bgn_form(FL_FLAT_BOX,init_len,init_thick);
	fl_set_form_background_color(form,BG_COLOR);
	fl_register_raw_callback(form,PointerMotionMask|EnterWindowMask|LeaveWindowMask|\
			KeyPressMask|KeyReleaseMask|ButtonPressMask|ButtonReleaseMask,handle_form);
	back = fl_add_free(FL_INACTIVE_FREE,0,0,init_len,init_thick,"",handle_free);
	fl_set_focus_object(form,back);
	fl_get_char_height(TSTYLE,TSIZE,&ta,&td);

	grp = fl_bgn_group();
	clabel = fl_add_text(FL_NORMAL_TEXT,0,0,TLEN*TW,TH+td,"");
	tlabel = fl_add_text(FL_NORMAL_TEXT,0,0,TLEN*TW,TH+td+ta,"");
	pixmap = fl_add_pixmap(FL_NORMAL_PIXMAP,0,0,PSIZE,PSIZE,"");
	fl_end_group();
	fl_hide_object(grp);
	fl_move_object(grp,-3,31);

	fl_set_object_resize(grp,FL_RESIZE_NONE);
	fl_set_object_gravity(grp,FL_West,FL_South);

	fl_set_object_color(tlabel,BG_COLOR,FG_COLOR);
	fl_set_object_lcolor(tlabel,FG_COLOR);
	fl_set_object_lsize(tlabel,TSIZE);
	fl_set_object_lstyle(tlabel,TSTYLE);

	fl_set_object_color(clabel,BG_COLOR,FG_COLOR);
	fl_set_object_lcolor(clabel,FG_COLOR);
	fl_set_object_lsize(clabel,TSIZE);
	fl_set_object_lstyle(clabel,TSTYLE);

	fl_set_object_color(pixmap,BG_COLOR,BG_COLOR);
	fl_set_object_boxtype(pixmap,FL_FLAT_BOX);
	fl_end_form();

	fl_prepare_form_window(form,FL_PLACE_FREE,FL_FULLBORDER,"XRuler");

	/* initial orientation setup */
	if(orient%2==0) {
		set_sizelimits(MIN_WIDTH,MIN_HEIGHT,MAX_WIDTH,MAX_HEIGHT);
		set_layout((orient==0)?XR_HU:XR_HD);
		fl_winresize(form->window,init_len,init_thick);
		fl_move_object(grp,0,(orient==0)?init_thick-30:1);
	} else if(orient%2==1) {
		set_sizelimits(MIN_HEIGHT,MIN_WIDTH,MAX_HEIGHT,MAX_WIDTH);
		set_layout((orient==1)?XR_VL:XR_VR);
		fl_winresize(form->window,init_thick,init_len);
		fl_move_object(grp,(orient==1)?-3:init_thick-TLEN*TW+3,init_thick-PSIZE-2*(TH+td)-ta-1);
	}

	/* window setup */
	fl_set_cursor(form->window,cur[orient]); /* set default cursor */
	utilitywindow(form->window); /* set window type to utility */
	windowlayer(form->window,XR_TOP); /* set top layer */
	undecorate(form->window); /* undecorate window */
	XSync(fl_display,False);

	fl_show_form_window(form);
	pix = XCreatePixmap(fl_display,form->window,PSIZE,PSIZE,32);
	gc = XCreateGC(fl_display,pix,0,NULL);

	fl_do_forms();

    fl_finish();

	return 0;
}
