PREFIX := /usr

xruler: xruler.c
	c99 -o xruler xruler.c -lforms -lX11 -Os -Wall -Fstrict -Wextra -pedantic -Werror
	strip xruler

install: xruler
	install -s $< $(PREFIX)/bin

clean:
	$(RM) xruler
