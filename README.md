# XRuler

Using the xforms toolkit and based on kruler, this small program aims to provide similar functionality but without the bloat of a Qt/KDE app.

![Screenshot](screenshot.png)

## Features
+ Orientation
+ Scale offset
+ Persistent markers (calipers)
+ Color picker with zoom
+ Transparency (under a compositing manager)
+ Color scheme (via X resources)
+ Keyboard move, offset and resize

## Command list
	<Button 1> -- move ruler
	<Button 2>/<Space> -- add marker
	<Button 3> -- color picker zoom
	q -- quit
	c -- clear markers
	r -- rotate
	f -- flip
	v -- toggle status
	y -- copy current color to clipboard (primary)
	l -- copy current length to clipboard (primary)
	+ -- increase window opacity
	- -- decrease window opacity
	<Shift> + + -- increase window layer (to front)
	<Shift> + - -- decrease window layer (to back)
	<MouseWheelUp> -- enlarge by 1px on right/bottom end
	<MouseWheelDn> -- shorten by 1px on right/bottom end
	<Shift> + <MouseWheelUp> -- enlarge by 1px on left/top end
	<Shift> + <MouseWheelDn> -- shorten by 1px on left/top end
	<Ctrl> + <MouseWheelUp> -- offset scale right by 1px
	<Ctrl> + <MouseWheelDn> -- offset scale left by 1px
	<ArrowKeys> -- move ruler (in corresponding direction)
	<Shift> + <ArrowKeys> -- resize ruler on right/bottom end (according to orientation)
	<Ctrl> + <ArrowKeys> -- offset scale (according to orientation)

## Command line options and X resources
	-offset | xruler.offset -- set initial offset
	-orientation | xruler.orientation -- set initial orientation
	-length | xruler.length -- set initial length
	-thickness | xruler.thickness -- set initial thickness
	-foreground | xruler.foreground -- set foreground color
	-background | xruler.background -- set background color
	-cursorColor | xruler.cursorColor -- set cursor color

## Usage details
* Current length is given by total ruler length if no markers, distance from left/top end to marker (inclusive) if one marker, and distance between markers (inclusive) if two markers. The idea of using inclusive distances is to measure sizes of objects on screen with the help of the color picker zoom as follows: using zoom locate pointer at initial pixel of object, set marker, using zoom locate pointer at final pixel of object, set marker, read length or copy to clipboard.

## Install
1. Install `xforms` (http://xforms-toolkit.org)
2. Run `make`
3. Install system-wide with `sudo install -m755 xruler /usr/bin`

## Disclaimer
This program is known to work properly under openbox and on a 32-bit TrueColor Visual, I'm not aware of things that could go wrong under a different setup. Bug reports and feature requests are welcome.

## Known bugs
+ When moving using `<Button 1>`, if another mouse button is clicked the window returns to the original location but keeps moving. I don't know how to address this, as related code was simply scraped from http://pastebin.com/4cbMeKJs.
+ Color picker zoom doesn't work on a 2px region near the screen borders, due to messy bounds checking. I don't think fixing this is worth the effort.

## Acknowledgements
* Initial motivation: `kruler` and the `xruler` at http://ishiboo.com/~danny/Projects/xruler/
* Ideas and code bits taken from: `kruler` (most notably the cursor shape and feature ideas), `transset-df` (transparency setting), `xforms` (inner workings and some xlib tricks), `devilspie` (undecorate routine), `xwininfo` (relative coordinates).
* Essential reference for Xlib programming: http://tronche.com/gui/x/xlib/
